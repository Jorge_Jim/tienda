<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'voku\\tests\\' => array($vendorDir . '/voku/portable-ascii/tests'),
    'voku\\' => array($vendorDir . '/voku/portable-ascii/src/voku'),
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/type-resolver/src', $vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'leifermendez\\installer\\' => array($baseDir . '/app/Class'),
    'XdgBaseDir\\' => array($vendorDir . '/dnoegel/php-xdg-base-dir/src'),
    'Whoops\\' => array($vendorDir . '/filp/whoops/src/Whoops'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Tymon\\JWTAuth\\' => array($vendorDir . '/tymon/jwt-auth/src'),
    'Twilio\\' => array($vendorDir . '/twilio/sdk/src/Twilio'),
    'Torann\\GeoIP\\' => array($vendorDir . '/torann/geoip/src'),
    'TijsVerkoyen\\CssToInlineStyles\\' => array($vendorDir . '/tijsverkoyen/css-to-inline-styles/src'),
    'Tests\\' => array($baseDir . '/tests'),
    'Symfony\\Polyfill\\Util\\' => array($vendorDir . '/symfony/polyfill-util'),
    'Symfony\\Polyfill\\Php73\\' => array($vendorDir . '/symfony/polyfill-php73'),
    'Symfony\\Polyfill\\Php72\\' => array($vendorDir . '/symfony/polyfill-php72'),
    'Symfony\\Polyfill\\Php56\\' => array($vendorDir . '/symfony/polyfill-php56'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Idn\\' => array($vendorDir . '/symfony/polyfill-intl-idn'),
    'Symfony\\Polyfill\\Iconv\\' => array($vendorDir . '/symfony/polyfill-iconv'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Contracts\\Translation\\' => array($vendorDir . '/symfony/translation-contracts'),
    'Symfony\\Contracts\\Service\\' => array($vendorDir . '/symfony/service-contracts'),
    'Symfony\\Contracts\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher-contracts'),
    'Symfony\\Component\\VarDumper\\' => array($vendorDir . '/symfony/var-dumper'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\Mime\\' => array($vendorDir . '/symfony/mime'),
    'Symfony\\Component\\HttpKernel\\' => array($vendorDir . '/symfony/http-kernel'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\ErrorHandler\\' => array($vendorDir . '/symfony/error-handler'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Swagger\\' => array($vendorDir . '/zircote/swagger-php/src'),
    'Svg\\' => array($vendorDir . '/phenx/php-svg-lib/src/Svg'),
    'Superbalist\\LaravelGoogleCloudStorage\\' => array($vendorDir . '/superbalist/laravel-google-cloud-storage/src'),
    'Superbalist\\Flysystem\\GoogleStorage\\' => array($vendorDir . '/superbalist/flysystem-google-storage/src'),
    'Spatie\\OpeningHours\\' => array($vendorDir . '/spatie/opening-hours/src'),
    'Sichikawa\\LaravelSendgridDriver\\' => array($vendorDir . '/s-ichikawa/laravel-sendgrid-driver/src'),
    'Ramsey\\Uuid\\' => array($vendorDir . '/ramsey/uuid/src'),
    'Ramsey\\Collection\\' => array($vendorDir . '/ramsey/collection/src'),
    'RachidLaasri\\LaravelInstaller\\' => array($vendorDir . '/leifermendez/laravel-installer/src'),
    'Psy\\' => array($vendorDir . '/psy/psysh/src'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\EventDispatcher\\' => array($vendorDir . '/psr/event-dispatcher/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src/Prophecy'),
    'Predis\\' => array($vendorDir . '/predis/predis/src'),
    'PhpParser\\' => array($vendorDir . '/nikic/php-parser/lib/PhpParser'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src/PhpOption'),
    'PhpOffice\\PhpSpreadsheet\\' => array($vendorDir . '/phpoffice/phpspreadsheet/src/PhpSpreadsheet'),
    'Opis\\Closure\\' => array($vendorDir . '/opis/closure/src'),
    'NunoMaduro\\Collision\\' => array($vendorDir . '/nunomaduro/collision/src'),
    'NotificationChannels\\Twilio\\' => array($vendorDir . '/laravel-notification-channels/twilio/src'),
    'Namshi\\JOSE\\' => array($vendorDir . '/namshi/jose/src/Namshi/JOSE'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'MaxMind\\WebService\\' => array($vendorDir . '/maxmind/web-service-common/src/WebService'),
    'MaxMind\\Exception\\' => array($vendorDir . '/maxmind/web-service-common/src/Exception'),
    'MaxMind\\Db\\' => array($vendorDir . '/maxmind-db/reader/src/MaxMind/Db'),
    'Matrix\\' => array($vendorDir . '/markbaker/matrix/classes/src'),
    'Madnest\\Madzipper\\' => array($vendorDir . '/madnest/madzipper/src/Madnest/Madzipper'),
    'Maatwebsite\\Excel\\' => array($vendorDir . '/maatwebsite/excel/src'),
    'League\\Flysystem\\Cached\\' => array($vendorDir . '/league/flysystem-cached-adapter/src'),
    'League\\Flysystem\\' => array($vendorDir . '/league/flysystem/src'),
    'League\\CommonMark\\' => array($vendorDir . '/league/commonmark/src'),
    'Lcobucci\\JWT\\' => array($vendorDir . '/lcobucci/jwt/src'),
    'Laravel\\Ui\\' => array($vendorDir . '/laravel/ui/src'),
    'Laravel\\Tinker\\' => array($vendorDir . '/laravel/tinker/src'),
    'Keygen\\' => array($vendorDir . '/gladcodes/keygen/src/Keygen'),
    'Ixudra\\Curl\\' => array($vendorDir . '/ixudra/curl/src'),
    'Intervention\\Image\\' => array($vendorDir . '/intervention/image/src/Intervention/Image'),
    'Illuminate\\Foundation\\Auth\\' => array($vendorDir . '/laravel/ui/auth-backend'),
    'Illuminate\\' => array($vendorDir . '/laravel/framework/src/Illuminate'),
    'Hashids\\' => array($vendorDir . '/hashids/hashids/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'Grpc\\Gcp\\' => array($vendorDir . '/google/grpc-gcp/src'),
    'Grpc\\' => array($vendorDir . '/grpc/grpc/src/lib'),
    'Google\\Protobuf\\' => array($vendorDir . '/google/protobuf/src/Google/Protobuf'),
    'Google\\Cloud\\Vision\\' => array($vendorDir . '/google/cloud-vision/src'),
    'Google\\Cloud\\Storage\\' => array($vendorDir . '/google/cloud-storage/src'),
    'Google\\Cloud\\Core\\' => array($vendorDir . '/google/cloud-core/src'),
    'Google\\CRC32\\' => array($vendorDir . '/google/crc32/src'),
    'Google\\Auth\\' => array($vendorDir . '/google/auth/src'),
    'Google\\ApiCore\\' => array($vendorDir . '/google/gax/src'),
    'Google\\' => array($vendorDir . '/google/common-protos/src'),
    'GeoIp2\\' => array($vendorDir . '/geoip2/geoip2/src'),
    'GeneaLabs\\LaravelPivotEvents\\' => array($vendorDir . '/genealabs/laravel-pivot-events/src'),
    'GeneaLabs\\LaravelModelCaching\\' => array($vendorDir . '/genealabs/laravel-model-caching/src'),
    'GPBMetadata\\Google\\Protobuf\\' => array($vendorDir . '/google/protobuf/src/GPBMetadata/Google/Protobuf'),
    'GPBMetadata\\Google\\Cloud\\Vision\\' => array($vendorDir . '/google/cloud-vision/metadata'),
    'GPBMetadata\\Google\\' => array($vendorDir . '/google/common-protos/metadata', $vendorDir . '/google/gax/metadata'),
    'Fruitcake\\Cors\\' => array($vendorDir . '/fruitcake/laravel-cors/src'),
    'FontLib\\' => array($vendorDir . '/phenx/php-font-lib/src/FontLib'),
    'Firebase\\JWT\\' => array($vendorDir . '/firebase/php-jwt/src'),
    'Fideloper\\Proxy\\' => array($vendorDir . '/fideloper/proxy/src'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'Facade\\Ignition\\' => array($vendorDir . '/facade/ignition/src'),
    'Facade\\IgnitionContracts\\' => array($vendorDir . '/facade/ignition-contracts/src'),
    'Facade\\FlareClient\\' => array($vendorDir . '/facade/flare-client-php/src'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/EmailValidator'),
    'Dotenv\\' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Dompdf\\' => array($vendorDir . '/dompdf/dompdf/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Doctrine\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Inflector'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib/Doctrine/Common/Lexer'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Common/Inflector'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib/Doctrine/Common/Annotations'),
    'DeepCopy\\' => array($vendorDir . '/myclabs/deep-copy/src/DeepCopy'),
    'Cron\\' => array($vendorDir . '/dragonmantank/cron-expression/src/Cron'),
    'Composer\\CaBundle\\' => array($vendorDir . '/composer/ca-bundle/src'),
    'Complex\\' => array($vendorDir . '/markbaker/complex/classes/src'),
    'Carbon\\' => array($vendorDir . '/nesbot/carbon/src/Carbon'),
    'Brick\\Math\\' => array($vendorDir . '/brick/math/src'),
    'Barryvdh\\DomPDF\\' => array($vendorDir . '/barryvdh/laravel-dompdf/src'),
    'Asm89\\Stack\\' => array($vendorDir . '/asm89/stack-cors/src/Asm89/Stack'),
    'App\\' => array($baseDir . '/app'),
    '' => array($vendorDir . '/google/grpc-gcp/src/generated'),
);
