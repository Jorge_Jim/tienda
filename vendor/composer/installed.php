<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'leifermendez/e-commerce',
  ),
  'versions' => 
  array (
    'asm89/stack-cors' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9c31def6a83f84b4d4a40d35996d375755f0e08',
    ),
    'barryvdh/laravel-dompdf' => 
    array (
      'pretty_version' => 'v0.8.6',
      'version' => '0.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd7108f78cf5254a2d8c224542967f133e5a6d4e8',
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.8.15',
      'version' => '0.8.15.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b08d412b9da9455b210459ff71414de7e6241cd',
    ),
    'composer/ca-bundle' => 
    array (
      'pretty_version' => '1.2.7',
      'version' => '1.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c63ab2117a72f48f5a55da9740a3273d45b7fd',
    ),
    'cordoval/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'davedevelopment/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'dnoegel/php-xdg-base-dir' => 
    array (
      'pretty_version' => 'v0.1.1',
      'version' => '0.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8f8a6e48c5ecb0f991c2fdcf5f154a47d85f9ffd',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9d758e831c70751155c698c2f7df4665314a1cb',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab5de36233a1995f9c776c741b803eb8207aebef',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae466f726242e637cebdd526a7d991b9433bacf1',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5242d66dbeb21a30dd8a3e66bf7a73b66e05e1f6',
    ),
    'dompdf/dompdf' => 
    array (
      'pretty_version' => 'v0.8.5',
      'version' => '0.8.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '6782abfc090b132134cd6cea0ec6d76f0fce2c56',
    ),
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '72b6fbf76adb3cf5bc0db68559b33d41219aba27',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '2.1.17',
      'version' => '2.1.17.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ade6887fd9bd74177769645ab5c474824f8a418a',
    ),
    'facade/flare-client-php' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db1e03426e7f9472c9ecd1092aff00f56aa6c004',
    ),
    'facade/ignition' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '67f1677954ad33ca6b77f2c41cf43a58624f27fc',
    ),
    'facade/ignition-contracts' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f445db0fb86f48e205787b2592840dd9c80ded28',
    ),
    'fideloper/proxy' => 
    array (
      'pretty_version' => '4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ec38ad69ee378a1eec04fb0e417a97cfaf7ed11a',
    ),
    'filp/whoops' => 
    array (
      'pretty_version' => '2.7.2',
      'version' => '2.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '17d0d3f266c8f925ebd035cd36f83cf802b47d4a',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v5.2.0',
      'version' => '5.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'feb0e820b8436873675fd3aca04f3728eb2185cb',
    ),
    'fruitcake/laravel-cors' => 
    array (
      'pretty_version' => 'v1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d127dbec313e2e227d65e0c483765d8d7559bf6',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'v1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc10d778e4b84d5bd315dad194661e091d307c6f',
    ),
    'genealabs/laravel-model-caching' => 
    array (
      'pretty_version' => '0.8.5',
      'version' => '0.8.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f0fd38601ed3537b00a5eaffc074db9371d6c3a8',
    ),
    'genealabs/laravel-pivot-events' => 
    array (
      'pretty_version' => '0.3.0',
      'version' => '0.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f35d5af019558474c0afc437bfa49ee161b658b',
    ),
    'geoip2/geoip2' => 
    array (
      'pretty_version' => 'v2.10.0',
      'version' => '2.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '419557cd21d9fe039721a83490701a58c8ce784a',
    ),
    'gladcodes/keygen' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '8806d4440edd37a5545802a82202098b01bbafb9',
    ),
    'google/auth' => 
    array (
      'pretty_version' => 'v1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c7b295feb248f138f462a1e6b7d635e4244204c5',
    ),
    'google/cloud-core' => 
    array (
      'pretty_version' => 'v1.36.2',
      'version' => '1.36.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ae49980a6c32758824838ca96205a1b22c8fc7a',
    ),
    'google/cloud-storage' => 
    array (
      'pretty_version' => 'v1.20.1',
      'version' => '1.20.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '876b5563555014563a3d8b2842674d95dacf95e1',
    ),
    'google/cloud-vision' => 
    array (
      'pretty_version' => 'v0.22.5',
      'version' => '0.22.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '34c84962e346ce9bae33228aeb1c4cfd7a3b2a56',
    ),
    'google/common-protos' => 
    array (
      'pretty_version' => '1.2',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e730d3567c8bfd94e06b538aa291270269ee3896',
    ),
    'google/crc32' => 
    array (
      'pretty_version' => 'v0.1.0',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8525f0dea6fca1893e1bae2f6e804c5f7d007fb',
    ),
    'google/gax' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa64abad3c748beca3b9564903c5ca1afe97bf02',
    ),
    'google/grpc-gcp' => 
    array (
      'pretty_version' => '0.1.4',
      'version' => '0.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e1ec44c0c39bd204868e2a2ed3cc87ed9006984e',
    ),
    'google/protobuf' => 
    array (
      'pretty_version' => 'v3.11.4',
      'version' => '3.11.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a5001eb559dcaac0bf6d8c7edc52b3828c618185',
    ),
    'grpc/grpc' => 
    array (
      'pretty_version' => '1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a461cb1a975dde305c73168aa4494b90c3801c62',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.3',
      'version' => '6.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aab4ebd862aa7d04f01a4b51849d657db56d882e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59da6cf61d80060647ff4d3eb2c03a2bc694646',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '239400de7a173fe9901b9ac7c06497751f00727a',
    ),
    'hamcrest/hamcrest-php' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '776503d3a8e85d4f9a1148614f95b7a608b046ad',
    ),
    'hashids/hashids' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b6c61142bfe36d43740a5419d11c351dddac0458',
    ),
    'illuminate/auth' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/broadcasting' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/bus' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/cache' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/config' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/console' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/container' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/cookie' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/database' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/encryption' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/events' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/hashing' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/http' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/log' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/mail' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/notifications' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/pagination' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/pipeline' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/queue' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/redis' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/routing' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/session' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/support' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/testing' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/translation' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/validation' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'illuminate/view' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.10.3',
      ),
    ),
    'intervention/image' => 
    array (
      'pretty_version' => '2.5.1',
      'version' => '2.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'abbf18d5ab8367f96b3205ca3c89fb2fa598c69e',
    ),
    'ixudra/curl' => 
    array (
      'pretty_version' => '6.19.0',
      'version' => '6.19.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08cb70241ae9ce26f5cb3e4f18366d11fa8a279b',
    ),
    'kodova/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'kylekatarnls/laravel-carbon-2' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0b68e0454d8e17b31cc42038dda8b7faef8055b3',
    ),
    'laravel-notification-channels/twilio' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f424ebce4fd587ffecf653246564899e930f9b4f',
    ),
    'laravel/framework' => 
    array (
      'pretty_version' => 'v7.10.3',
      'version' => '7.10.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e927e78aafd578d59c99608e7f0e23a5f7bfc5a',
    ),
    'laravel/tinker' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cde90a7335a2130a4488beb68f4b2141869241db',
    ),
    'laravel/ui' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '15368c5328efb7ce94f35ca750acde9b496ab1b1',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a11ec5f4b4d75d1fcd04e133dede4c317aac9e18',
    ),
    'league/commonmark' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '412639f7cfbc0b31ad2455b2fe965095f66ae505',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.0.67',
      'version' => '1.0.67.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b1f36c75c4bdde981294c2a0ebdb437ee6f275e',
    ),
    'league/flysystem-cached-adapter' => 
    array (
      'pretty_version' => '1.0.9',
      'version' => '1.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '08ef74e9be88100807a3b92cc9048a312bf01d6f',
    ),
    'leifermendez/e-commerce' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'leifermendez/laravel-installer' => 
    array (
      'pretty_version' => '4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9f2a794a37240e03d8ba5aae1ab9edb2bde3ac2',
    ),
    'maatwebsite/excel' => 
    array (
      'pretty_version' => '3.1.19',
      'version' => '3.1.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '96527a9ebc2e79e9a5fa7eaef7e23c9e9bcc587c',
    ),
    'madnest/madzipper' => 
    array (
      'pretty_version' => 'v1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '268ca799d22cc7576f0c952fb2f4a891a9bb590a',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '1.4.8',
      'version' => '1.4.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '8eaa40cceec7bf0518187530b2e63871be661b72',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5348c5a67e3b75cd209d70103f916a93b1f1ed21',
    ),
    'maxmind-db/reader' => 
    array (
      'pretty_version' => 'v1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'febd4920bf17c1da84cef58e56a8227dfb37fbe4',
    ),
    'maxmind/web-service-common' => 
    array (
      'pretty_version' => 'v0.7.0',
      'version' => '0.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '74c996c218ada5c639c8c2f076756e059f5552fc',
    ),
    'mockery/mockery' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f69bbde7d7a75d6b2862d9ca8fab1cd28014b4be',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c861fcba2ca29404dc9e617eedd9eff4616986b8',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.9.5',
      'version' => '1.9.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b2c28789e80a97badd14145fda39b545d83ca3ef',
      'replaced' => 
      array (
        0 => '1.9.5',
      ),
    ),
    'namshi/jose' => 
    array (
      'pretty_version' => '7.2.3',
      'version' => '7.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '89a24d7eb3040e285dd5925fcad992378b82bcff',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.33.0',
      'version' => '2.33.0.0',
      'aliases' => 
      array (
        0 => '1.39.0',
      ),
      'reference' => '4d93cb95a80d9ffbff4018fe58ae3b7dd7f4b99b',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.4.0',
      'version' => '4.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bd43ec7152eaaab3bd8c6d0aa95ceeb1df8ee120',
    ),
    'nunomaduro/collision' => 
    array (
      'pretty_version' => 'v4.2.0',
      'version' => '4.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd50490417eded97be300a92cd7df7badc37a9018',
    ),
    'opis/closure' => 
    array (
      'pretty_version' => '3.5.1',
      'version' => '3.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '93ebc5712cdad8d5f489b500c59d122df2e53969',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '45a2ec53a73c70ce41d55cedef9063630abaf1b6',
    ),
    'phenx/php-font-lib' => 
    array (
      'pretty_version' => '0.5.2',
      'version' => '0.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca6ad461f032145fff5971b5985e5af9e7fa88d8',
    ),
    'phenx/php-svg-lib' => 
    array (
      'pretty_version' => 'v0.3.3',
      'version' => '0.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa61b65e612ce1ae15f69b3d223cb14ecc60e32',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6568f4687e5b41b054365f9ae03fcb1ed5f2069b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.1.0',
      'version' => '5.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd72d394ca794d3466a3b2fc09d5a6c1dc86b47e',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7462d5f123dfc080dfdf26897032a6513644fc95',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.12.0',
      'version' => '1.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f79611d6dc1f6b7e8e30b738fc371b392001dbfd',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => '1.7.3',
      'version' => '1.7.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4acfd6a4b33a509d8c88f50e5222f734b6aeebae',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => 'v1.10.3',
      'version' => '1.10.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '451c3cd1418cf640de218914901e51b064abb093',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '7.0.10',
      'version' => '7.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f1884187926fbb755a9aaf0b3836ad3165b478bf',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '050bedf145a257b1ff02746c31894800e5122946',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1038454804406b0b5f5f520358e78c1c2f71501e',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '995192df77f63a59e47f025390d2d1fdf8f425ff',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '8.5.4',
      'version' => '8.5.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '8474e22d7d642f665084ba5ec780626cbd1efd23',
    ),
    'predis/predis' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f0210e38881631afeafb56ab43405a92cafd9fd1',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
        1 => '1.0.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.10.4',
      'version' => '0.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8aec1b2981ab66882a01cce36a49b6317dc3560',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'ramsey/collection' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '925ad8cf55ba7a3fc92e332c58fd0478ace3e1ca',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '4.0.1',
      'version' => '4.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba8fff1d3abb8bb4d35a135ed22a31c6ef3ede3d',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '4.0.1',
      ),
    ),
    'rize/uri-template' => 
    array (
      'pretty_version' => '0.3.2',
      'version' => '0.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9e5fdd5c47147aa5adf7f760002ee591ed37b9ca',
    ),
    's-ichikawa/laravel-sendgrid-driver' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '116a6e624323883ddcc56bfaf92ef19f9218f6fd',
    ),
    'sabberworm/php-css-parser' => 
    array (
      'pretty_version' => '8.3.0',
      'version' => '8.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '91bcc3e3fdb7386c9a2e0e0aa09ca75cc43f121f',
    ),
    'scrivo/highlight.php' => 
    array (
      'pretty_version' => 'v9.18.1.1',
      'version' => '9.18.1.1',
      'aliases' => 
      array (
      ),
      'reference' => '52fc21c99fd888e33aed4879e55a3646f8d40558',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5de4fc177adf9bce8df98d8d141a7559d7ccf6da',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '720fcc7e9b5cf384ea68d9d930d480907a0c1a29',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.3',
      'version' => '4.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '464c90d7bdf5ad4e8a6aea15c091fec0603d4368',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '68609e1261d215ea5b21b7987539cbfbe156ec3e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edf8a461cf1d4005f19fb0b6b8b95a9f7fa0adc4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cfd9e65d11ffb5af41198476395774d4c8a84c5',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '773f97c67f28de00d397be301821b06708fca0be',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d7a795d35b889bf80a0cc04e08d77cedfa917a9',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3aaaa15fa71d27650d62a948be022fe3b48541a3',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'spatie/opening-hours' => 
    array (
      'pretty_version' => '2.7.0',
      'version' => '2.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bac0ed7d3dcea5d6f048fce6da1b7fe3aeffdccf',
    ),
    'superbalist/flysystem-google-storage' => 
    array (
      'pretty_version' => '7.2.2',
      'version' => '7.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '87e2f450c0e4b5200fef9ffe6863068cc873d734',
    ),
    'superbalist/laravel-google-cloud-storage' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '588345471e3f56f8b7119c83a0637fa33897bcc0',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.2.3',
      'version' => '6.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '149cfdf118b169f7840bbe3ef0d4bc795d1780c9',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa1caadc8cdaa17bcfb25219f3b53fe294a9935',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f8d5271303dad260692ba73dfa21777d38e124e',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '949ffc17c3ac3a9f8e6232220e2da33913c04ea4',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '24f40d95385774ed5c71dbf014edd047e2f2f3dc',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af23c2584d4577d54661c434446fb8fbed6025dd',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '600a52c29afc0d1caa74acbec8d3095ca7e9910d',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e47fdf8b24edc12022ba52923150ec6484d7f57d',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '3565e51eecd06106304baba5ccb7ba89db2d7d2b',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d6c81c39225a750f3f43bee15f03093fb9aaa0b',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1aab00e39cebaef4d8652497f46c15c1b7e45294',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd51debc1391a609c514f6f072dd59a61b461502a',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab0af41deab94ec8dceb3d1fb408bdd038eba4dc',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a54881ec0ab3b2005c406aed0023c062879031e7',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '14b55b0e88c67c6a14526799abaea197dde78911',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '42fda6d7380e5c940d7f68341ccae89d5ab9963b',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e95fe59d12169fcf4041487e4bf34fca37ee0ed',
    ),
    'symfony/polyfill-util' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa1fdaf94e8a60932d8821692eb1ed07efc52db2',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '3179f68dff5bad14d38c4114a1dab98030801fd7',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b18480a6e101f8d9ab7c483ace7c19441be5111',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '144c5e51266b281231e947b51223ba14acf1a749',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3879db7a68fe3e12b41263b05879412c87b27fd',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8cc682ac458d75557203b2f2f14b0b92e1c744ed',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '09de28632f16f81058a85fcf318397218272a07b',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '11336f6f84e16a720dae9d8e6ed5019efa85a0f9',
    ),
    'tijsverkoyen/css-to-inline-styles' => 
    array (
      'pretty_version' => '2.2.2',
      'version' => '2.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dda2ee426acd6d801d5b7fd1001cde9b5f790e15',
    ),
    'torann/geoip' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '15c7cb3d2edcfbfd7e8cd6f435defc2352df40d2',
    ),
    'twilio/sdk' => 
    array (
      'pretty_version' => '5.42.2',
      'version' => '5.42.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0cfcb871b18a9c427dd9e8f0ed7458d43009b48a',
    ),
    'tymon/jwt-auth' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd4cf9fd2b98790712d3e6cd1094e5ff018431f19',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v4.1.5',
      'version' => '4.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '539bb6927c101a5605d31d11a2d17185a2ce2bf1',
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.4.10',
      'version' => '1.4.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '240e93829a5f985fab0984a6e55ae5e26b78a334',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab2cb0b3b559010b75981b1bdce728da3ee90ad6',
    ),
    'zircote/swagger-php' => 
    array (
      'pretty_version' => '2.0.15',
      'version' => '2.0.15.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fd9439cfb76713925e23f206e9db4bf35784683',
    ),
  ),
);
